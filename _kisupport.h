/* KInterbasDB Python Package - Header File for Support
 *
 * Version 3.3
 *
 * The following contributors hold Copyright (C) over their respective
 * portions of code (see license.txt for details):
 *
 * [Original Author (maintained through version 2.0-0.3.1):]
 *   1998-2001 [alex]  Alexander Kuznetsov   <alexan@users.sourceforge.net>
 * [Maintainers (after version 2.0-0.3.1):]
 *   2001-2002 [maz]   Marek Isalski         <kinterbasdb@maz.nu>
 *   2002-2007 [dsr]   David Rushby          <woodsplitter@rocketmail.com>
 * [Contributors:]
 *   2001      [eac]   Evgeny A. Cherkashin  <eugeneai@icc.ru>
 *   2001-2002 [janez] Janez Jere            <janez.jere@void.si>            */

#ifndef _KISUPPORT_H
#define _KISUPPORT_H

#include "_kisupport_platform.h"

#ifdef ENABLE_CONNECTION_TIMEOUT
static PlatformThreadRefType Thread_current_ref(void);
#endif /* ENABLE_CONNECTION_TIMEOUT */

static PlatformThreadIdType Thread_current_id(void);
static boolean Thread_ids_equal(
    PlatformThreadIdType a, PlatformThreadIdType b
  );

static PlatformThreadRefType Thread_create(
    PlatformThreadFuncType, void *, PlatformThreadIdType *
  );
static long Thread_join(PlatformThreadRefType);

#ifdef ENABLE_CONNECTION_TIMEOUT
static void sleep_millis(unsigned int millis);
#endif /* ENABLE_CONNECTION_TIMEOUT */

static long Mutex_init(PlatformMutexType *);
static long Mutex_close(PlatformMutexType *);
static long Mutex_lock(PlatformMutexType *);
static long Mutex_unlock(PlatformMutexType *);

typedef enum {
  WR_WAIT_CANCELLED = -2,
  WR_WAIT_ERROR     = -1,
  WR_WAIT_OK        =  0,
  WR_WAIT_TIMEOUT   =  1
} WaitResult;

#define WAIT_INFINITELY_FLOAT -1.0
#define WAIT_INFINITELY_LONG -1
#define MILLI_TIMEOUT_IS_INFINITE(millis) ((millis) == WAIT_INFINITELY_LONG)

#endif /* if not def _KISUPPORT_H */
