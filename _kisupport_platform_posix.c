/* KInterbasDB Python Package - Implementation of Platform Infrastructure for
 *                                                POSIX
 *
 * Version 3.3
 *
 * The following contributors hold Copyright (C) over their respective
 * portions of code (see license.txt for details):
 *
 * [Original Author (maintained through version 2.0-0.3.1):]
 *   1998-2001 [alex]  Alexander Kuznetsov   <alexan@users.sourceforge.net>
 * [Maintainers (after version 2.0-0.3.1):]
 *   2001-2002 [maz]   Marek Isalski         <kinterbasdb@maz.nu>
 *   2002-2007 [dsr]   David Rushby          <woodsplitter@rocketmail.com>
 * [Contributors:]
 *   2001      [eac]   Evgeny A. Cherkashin  <eugeneai@icc.ru>
 *   2001-2002 [janez] Janez Jere            <janez.jere@void.si>            */

/* This source file is designed to be directly included in _kievents.c,
 * without the involvement of a header file. */

/* NOTE: THE CODE IN THIS FILE IS TYPICALLY EXECUTED WHEN THE GIL IS NOT HELD,
 * SO IT MUST NOT CALL THE PYTHON C API! */

/* With pthreads, there's no difference between a thread reference and a thread
 * ID. */
static PlatformThreadRefType Thread_current_ref() {
  return pthread_self();
} /* Thread_current_ref */

static PlatformThreadIdType Thread_current_id() {
  return pthread_self();
} /* Thread_current_id */

static boolean Thread_ids_equal(
    PlatformThreadIdType a, PlatformThreadIdType b
  )
{
  return (pthread_equal(a, b) != 0);
} /* Thread_ids_equal */

static PlatformThreadRefType Thread_create(
    PlatformThreadFuncType func, void *func_arg,
    PlatformThreadIdType *store_thread_id
  )
{
  /* Unlike on Windows, PlatformThreadRefType and the thread ID type are the
   * same with pthreads. */
  int status = pthread_create(store_thread_id, NULL, func, func_arg);
  return status == 0 ? *store_thread_id : THREAD_REF_INVALID;
} /* Thread_create */

static long Thread_join(PlatformThreadRefType t) {
  return pthread_join(t, NULL);
} /* Thread_join */

static void sleep_millis(unsigned int millis) {
  const unsigned int seconds = millis / 1000;
  const unsigned int useconds = (millis % 1000) * 1000;

  if (seconds != 0) {
    sleep(seconds);
  }
  if (useconds != 0) {
    usleep((useconds_t) useconds);
  }
} /* sleep_millis */

static long Mutex_init(PlatformMutexType *m) {
  return (pthread_mutex_init(m, NULL) == 0 ? 0 : -1);
} /* Mutex_initialize */

static long Mutex_close(PlatformMutexType *m) {
  return (pthread_mutex_destroy(m) == 0 ? 0 : -1);
} /* Mutex_close */

static long Mutex_lock(PlatformMutexType *m) {
  return (pthread_mutex_lock(m) == 0 ? 0 : -1);
} /* Mutex_lock */

static long Mutex_unlock(PlatformMutexType *m) {
  return (pthread_mutex_unlock(m) == 0 ? 0 : -1);
} /* Mutex_unlock */

static void millis_into_future_to_abstime(
    long millis, struct timespec *abstime
  )
{
  struct timeval now;
  const long rel_secs = millis / 1000;
  const long rel_millis = millis % 1000;
  const long rel_nanos = rel_millis * 1000000;

  /* 1: use $now to get the absolute time: */
  gettimeofday(&now, NULL);
  /* 2: transfer the values from $now to $abstime: */
  abstime->tv_sec = now.tv_sec;
  abstime->tv_nsec = now.tv_usec * 1000;
  /* 3: add the relative timeout to $abstime */
  abstime->tv_sec += rel_secs;
  {
    const long total_nanos = abstime->tv_nsec + rel_nanos;
    abstime->tv_sec += total_nanos / 1000000000;
    abstime->tv_nsec = total_nanos % 1000000000;
  }
} /* millis_into_future_to_abstime */
