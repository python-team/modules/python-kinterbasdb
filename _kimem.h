/* KInterbasDB Python Package - Header File for Memory Management Wrappers
 *
 * Version 3.3
 *
 * The following contributors hold Copyright (C) over their respective
 * portions of code (see license.txt for details):
 *
 * [Original Author (maintained through version 2.0-0.3.1):]
 *   1998-2001 [alex]  Alexander Kuznetsov   <alexan@users.sourceforge.net>
 * [Maintainers (after version 2.0-0.3.1):]
 *   2001-2002 [maz]   Marek Isalski         <kinterbasdb@maz.nu>
 *   2002-2007 [dsr]   David Rushby          <woodsplitter@rocketmail.com>
 * [Contributors:]
 *   2001      [eac]   Evgeny A. Cherkashin  <eugeneai@icc.ru>
 *   2001-2002 [janez] Janez Jere            <janez.jere@void.si>
 */

/* This header file is intended to provide centralized aliases for the various
 * sets of memory handling functions that kinterbasdb uses.  The centralization
 * will have two main benefits:
 * - facilitate debug tracing of memory operations
 * - reduce the likelihood of "series mismatches" between allocation and
 *   freeing functions for the same piece of memory by providing more
 *   descriptive names (such as kimem_xsqlda_malloc for XSQLDA memory
 *   allocation).
 *     This issue is especially crucial with Python 2.3, which makes the
 *   specialized pymalloc memory allocater (available via the
 *   PyObject_[Malloc|Realloc|Free] series) the default. */

#ifndef _KIMEM_H
#define _KIMEM_H

/***************************     PLAIN     ***********************************/

/* kimem_plain_* is kinterbasdb's simplest series of memory handlers.
 * Typically, these will simply be aliases for the libc C memory handlers.
 *
 * kimem_plain_* should NEVER resolve to pymalloc's memory handlers, nor to any
 * other memory allocator that's expected to be incompatible with "generic"
 * memory allocated "by some third party".
 *
 * Also, unlike kimem_main_*, this series must be threadsafe.                */
#define kimem_plain_malloc          malloc
#define kimem_plain_realloc         realloc
#define kimem_plain_free            free


/***************************     MAIN      ***********************************/

/* Series for pymalloc, the specialized Python-oriented memory handler that
 * became standard in Python 2.3.
 *
 * Unless there's a specific reason not to (as noted elsewhere in this file,
 * including in the WARNING just below), any kinterbasdb [|de|re]allocation of
 * raw memory should use this series.
 *
 * WARNING:
 *   Members of the kimem_main_* series must only be called when the GIL is
 * held, since they rely on pymalloc, which assumes the GIL is held.         */
#define kimem_main_malloc           PyObject_Malloc
#define kimem_main_realloc          PyObject_Realloc
#define kimem_main_free             PyObject_Free


/***************************    DB_CLIENT   **********************************/

/* This series is implemented by the database client library.  The client
 * library sometimes gives us deallocation responsibility for chunks of memory
 * that were allocated using its own memory handler. */
#define kimem_db_client_malloc          isc_malloc
#define kimem_db_client_realloc         isc_realloc
#define kimem_db_client_free            isc_free


/***************************    XSQLDA     ***********************************/

/* The kimem_xsqlda_* memory management aliases were established because
 * trouble arises when XSQLDA structures are allocated/freed with pymalloc.
 * Probably, some isc_* functions that handle XSQLDAs or their XSQLVARs make
 * changes to those structures' memory that are not obvious, and in a way that
 * requires those structures to have been allocated with the standard C malloc.
 *
 * NOTES:
 * - These memory handlers are for the XSQLDA structures themselves (which
 *   contain XSQLVARs), not for handling memory indirectly related to the
 *   XSQLDA, such as XSQLVAR.sqldata and XSQLVAR.sqlind (those latter work just
 *   fine with pymalloc). */
#define kimem_xsqlda_malloc         kimem_plain_malloc
#define kimem_xsqlda_realloc        kimem_plain_realloc
#define kimem_xsqlda_free           kimem_plain_free

/*****************************************************************************/

#endif /* not def _KIMEM_H */
