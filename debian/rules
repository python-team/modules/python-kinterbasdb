#!/usr/bin/make -f
# Sample debian/rules that uses debhelper.
# GNU copyright 1997 to 1999 by Joey Hess.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include /usr/share/python/python.mk

ifneq (,$(findstring debug,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -g
endif
ifeq (,$(findstring nostrip,$(DEB_BUILD_OPTIONS)))
	INSTALL_PROGRAM += -s
endif

PYVERS=$(shell pyversions -r)

build build-arch: build-stamp
build-stamp:
	dh_testdir

	set -e; \
	for py in $(PYVERS); do $$py setup.py build; $$py-dbg setup.py build; done

	touch build-stamp

build-indep:

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp configure-stamp

	-for py in $(PYVERS); do $$py setup.py clean ; $$py-dbg setup.py clean ; done
	
	[ ! -d build ] || rm -rf build

	dh_clean __ki_platform_config.h

	# restore __init__.py with default compilation time
	sed -i "s/__timestamp__.*/__timestamp__ = \'2009\.01\.13\.11\.52\.28\.UTC\'/" __init__.py

install: build
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs

	set -e; \
	for py in $(PYVERS); do \
	    $$py setup.py install --root=debian/python-kinterbasdb $(py_setup_install_args); \
	    $$py-dbg setup.py install --root=debian/python-kinterbasdb-dbg $(py_setup_install_args); \
	done

	-find debian -name '*.py[co]' | xargs rm -f

	# these are wrongly installed by the script above:
	rm -rf debian/python-kinterbasdb/usr/lib/python*/*-packages/kinterbasdb/docs
	rm -rf debian/python-kinterbasdb-dbg/usr/lib/python*/*-packages/kinterbasdb/docs
	rm -rf debian/python-kinterbasdb/usr/lib/python2.6/site-packages

	# remove things not needed in a debug package
	find debian/python-kinterbasdb-dbg ! -type d ! -name '*_d.so' | xargs rm -f
	find debian/python-kinterbasdb-dbg -depth -empty -exec rmdir {} \;

# Build architecture-independent files here.
binary-indep: install

# Build architecture-dependent files here.
binary-arch: install
	dh_testdir
	dh_testroot
	# remove duplicate licence text in doc
	rm -f docs/_sources/license.txt
	dh_installdocs		docs
	# generate text upstream changelog, install it and remove it
	lynx -dump docs/changelog.html > docs/changelog.txt
	dh_installchangelogs	docs/changelog.txt
	rm -f docs/changelog.txt
	# remove embedded jquery.js and use the system one
	rm -f $(CURDIR)/debian/python-kinterbasdb/usr/share/doc/python-kinterbasdb/docs/_static/jquery.js
	ln -sf /usr/share/javascript/jquery/jquery.js \
		$(CURDIR)/debian/python-kinterbasdb/usr/share/doc/python-kinterbasdb/docs/_static/jquery.js
	dh_link
	dh_strip -ppython-kinterbasdb --dbg-package=python-kinterbasdb-dbg
	dh_compress -X.js -Xobjects.inv
	dh_fixperms
	dh_python2
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install
