/* KInterbasDB Python Package - Header File for ThreadSafeFIFOQueue
 *
 * Version 3.3
 *
 * The following contributors hold Copyright (C) over their respective
 * portions of code (see license.txt for details):
 *
 * [Original Author (maintained through version 2.0-0.3.1):]
 *   1998-2001 [alex]  Alexander Kuznetsov   <alexan@users.sourceforge.net>
 * [Maintainers (after version 2.0-0.3.1):]
 *   2001-2002 [maz]   Marek Isalski         <kinterbasdb@maz.nu>
 *   2002-2007 [dsr]   David Rushby          <woodsplitter@rocketmail.com>
 * [Contributors:]
 *   2001      [eac]   Evgeny A. Cherkashin  <eugeneai@icc.ru>
 *   2001-2002 [janez] Janez Jere            <janez.jere@void.si>            */

#ifndef _KISUPPORT_THREADSAFE_FIFO_QUEUE_H
#define _KISUPPORT_THREADSAFE_FIFO_QUEUE_H

#include "_kisupport.h"

typedef void (*QueueNodeDelFunc)(void *);

typedef struct _QueueNode {
  volatile void *payload;
  volatile QueueNodeDelFunc payload_del_func;

  volatile struct _QueueNode *next;
} QueueNode;

typedef struct {
  PlatformMutexType lock;

  /* There are so many subtle differences between pthread_cond_t objects and
   * Windows Event objects that it's less error-prone to ifdef the operations
   * inline than to try to build a uniform abstraction.
   * Clients of the queue aren't aware of what synch primitives it's using
   * under the hood anyway, so the platform-specificity doesn't leak into other
   * parts of the code base. */
  #ifdef PLATFORM_WINDOWS
    HANDLE
  #else
    pthread_cond_t
  #endif
  not_empty;

  volatile boolean cancelled;
  volatile boolean closed;

  volatile QueueNode *head;
  volatile QueueNode *tail;
} ThreadSafeFIFOQueue;

#endif /* if not def _KISUPPORT_THREADSAFE_FIFO_QUEUE_H */
