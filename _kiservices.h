/* KInterbasDB Python Package - Header File for Services Manager Support
 *
 * Version 3.3
 *
 * The following contributors hold Copyright (C) over their respective
 * portions of code (see license.txt for details):
 *
 * [Original Author (maintained through version 2.0-0.3.1):]
 *   1998-2001 [alex]  Alexander Kuznetsov   <alexan@users.sourceforge.net>
 * [Maintainers (after version 2.0-0.3.1):]
 *   2001-2002 [maz]   Marek Isalski         <kinterbasdb@maz.nu>
 *   2002-2007 [dsr]   David Rushby          <woodsplitter@rocketmail.com>
 * [Contributors:]
 *   2001      [eac]   Evgeny A. Cherkashin  <eugeneai@icc.ru>
 *   2001-2002 [janez] Janez Jere            <janez.jere@void.si>
 */

#ifndef _KISERVICES_H
#define _KISERVICES_H

#include "_kinterbasdb.h"

static PyTypeObject ServicesConnectionType;

typedef struct { /* definition of type ServicesConnectionObject */
  PyObject_HEAD /* Python API - infrastructural macro. */

  isc_svc_handle service_handle;

  /* Buffer used by Interbase API to store error status of calls. */
  ISC_STATUS status[STATUS_VECTOR_SIZE];

} ServicesConnectionObject;

#endif /* not def _KISERVICES_H */
